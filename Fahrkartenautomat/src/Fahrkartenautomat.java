import java.util.Scanner;

class Fahrkartenautomat
{
    static final double EINZELFAHRSCHEIN = 2.90;
    static final double TAGESKARTE = 8.60;
    static final double KLEINGRUPPENKARTE = 23.50;

    public static double eingabeTastatur() {
        double eingabe;

        while(true) {
            try{
                Scanner tastatur = new Scanner(System.in); //Für Werte welche aus Benutzereingaben erzeugt werden
                eingabe = tastatur.nextDouble();
                return eingabe;
            } catch (Exception eric) {
                System.out.println("Bitte eine richtige Zahl eingeben.");
            }
        }
    }

    public static String fahrkartenBezahlen(double a) {
        if(a > 0.00)
        {
            System.out.println("Der Rückgabebetrag in Höhe von " + String.format("%.2f", a) + " EURO");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            a = Math.round(a*100D)/100D; //rundet die Variable, damit einige While-Bedingungen true sein können
            //ohne Rundung sind 5 CENT Beträge z.B. 0,049999, sodass sie nicht in der letzten While-Schleife wahr sind

            while(a >= 2.00) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                a -= 2.00;
                a = Math.round(a*100D)/100D;  //Variable a wird gerundet, damit die Rückgabebeträge korrekt ausgegeben werden. Java macht Rundungsfehler.
            }
            while(a >= 1.00) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                a -= 1.00;
                a = Math.round(a*100D)/100D;  //Variable a wird gerundet, damit die Rückgabebeträge korrekt ausgegeben werden. Java macht Rundungsfehler.
            }
            while(a >= 0.50) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                a -= 0.50;
                a = Math.round(a*100D)/100D;  //Variable a wird gerundet, damit die Rückgabebeträge korrekt ausgegeben werden. Java macht Rundungsfehler.
            }
            while(a >= 0.20) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                a -= 0.20;
                a = Math.round(a*100D)/100D;  //Variable a wird gerundet, damit die Rückgabebeträge korrekt ausgegeben werden. Java macht Rundungsfehler.
            }
            while(a >= 0.10) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                a -= 0.10;
                a = Math.round(a*100D)/100D;  //Variable a wird gerundet, damit die Rückgabebeträge korrekt ausgegeben werden. Java macht Rundungsfehler.
            }
            while(a >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                a -= 0.05;
                a = Math.round(a*100D)/100D;  //Variable a wird gerundet, damit die Rückgabebeträge korrekt ausgegeben werden. Java macht Rundungsfehler.
            }
        }
        return " ";
    }

    public static double anzahlFahrkarten() {
        System.out.println("Anzahl der Fahrkarten eingeben: ");
        while(true) {
            double anzahlFahrkarten = eingabeTastatur();
            anzahlFahrkarten = Math.round(anzahlFahrkarten);
            if(anzahlFahrkarten > 10) {
                System.out.println("Maximal 10 Fahrkarten bestellbar!");
                System.out.println("Anzahl der Fahrkarten eingeben: ");
            }
            else if(anzahlFahrkarten < 1) {
                System.out.println("Mindestens 1 Fahrkarte bestellen!");
                System.out.println("Anzahl der Fahrkarten eingeben: ");
            }
            else {
                return anzahlFahrkarten;
            }
        }

    }

    public static double berechnungZahlung(double a) {
        double eingezahlterGesamtbetrag = 0;
        while(eingezahlterGesamtbetrag < a)
        {
            String nochZuZahlen = "Noch zu zahlen: " + (String.format("%.2f",a - eingezahlterGesamtbetrag)) + " EURO";
            String eingabeAufforderung = "Eingabe (mind. 5Ct, höchstens 2 Euro): ";
            System.out.println(nochZuZahlen);
            System.out.print(eingabeAufforderung);
            double b = eingabeTastatur();  //Methodenaufruf Scanner für einzuwerfende Münzen
            while(b < 0 || b > 2) {
                System.out.println("Bitte gültigen Betrag eingeben!");
                System.out.println(nochZuZahlen);
                System.out.print(eingabeAufforderung);
                b = eingabeTastatur();  //Methodenaufruf Scanner
            }
            eingezahlterGesamtbetrag += b;
        }
        return eingezahlterGesamtbetrag;
    }

    public static void fahrscheinausgabe() {
        System.out.println("\nFahrscheine werden ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static double fahrkartenBestellungErfassen() {
        double zwischenSumme;
        double zuZahlenderBetrag = 0;
        int modusAutomat;

        System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
        System.out.println("(1)Einzelfahrschein Regeltarif AB [2,90 EUR]");
        System.out.println("(2)Tageskarte Regeltarif AB [8,60 EUR]");
        System.out.println("(3)Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR]");


        while(true) {
            System.out.println("Wählen Sie: ");
            System.out.println("(1) Einzelfahrscheine");
            System.out.println("(2) Tageskarten");
            System.out.println("(3) Kleingruppen-Tageskarten");
            System.out.println("(9) Bezahlen");
            System.out.print("Bitte einen Vorgang auswählen: ");
            modusAutomat = (int) eingabeTastatur();

            switch (modusAutomat) {  //Notiz an Lehrer: IntelliJ hat mir "enhanced switch" als Fix vorgeschlagen
                case (1) -> {
                    System.out.println("Sie haben Einzelfahrschein(e) ausgewählt. ");
                    zwischenSumme = anzahlFahrkarten() * EINZELFAHRSCHEIN;
                    zuZahlenderBetrag += zwischenSumme;
                    System.out.println("Sie haben " + zwischenSumme / EINZELFAHRSCHEIN + " Einzelfahrschein(e) ausgewählt.");
                }
                case (2) -> {
                    zwischenSumme = TAGESKARTE * anzahlFahrkarten();
                    zuZahlenderBetrag += zwischenSumme;
                    System.out.println("Sie haben " + zwischenSumme / TAGESKARTE + " Tageskarte(n) ausgewählt.");
                }
                case (3) -> {
                    zwischenSumme = KLEINGRUPPENKARTE * anzahlFahrkarten();
                    zuZahlenderBetrag += zwischenSumme;
                    System.out.println("Sie haben " + zwischenSumme / KLEINGRUPPENKARTE + " Kleingruppen-Tageskarte(n) ausgewählt.");
                }
                case 9 -> {
                    return zuZahlenderBetrag;
                }
                default -> System.out.println("Bitte einen der Vorgänge auswählen [1][2][3][9]: ");
            }
        }
    }



    public static void main(String[] args)
    {
        double zuZahlenderBetragMain;		//Variable, Datentyp Double, wird erst eingelesen, multipliziert mit
        // anzahlFahrkarten, vom eingezhalterGesamtbetrag subtrahiert und ausgegeben
        double rueckgabebetrag;			//Variable, Datentyp Double, wird gerundet und in if- und while-Abfragen nach
        // verschiedenen Bedingungen überprüft und ausgegeben
        double eingezahlterGesamtbetrag;  //Variable, Datentyp Double, wird mit zuZahlenderBetragMain subtrahiert und ausgegeben

        while(true) {
            zuZahlenderBetragMain = fahrkartenBestellungErfassen();
            eingezahlterGesamtbetrag = berechnungZahlung(zuZahlenderBetragMain); //Methodenaufruf Zahlungsberechnung,
            // sorgt dafür, dass nichts größeres als 2€ und nichts kleineres als 5ct genutzt wird
            fahrscheinausgabe(); //Methodenaufruf Ausgabe Fahrscheine druckt Zeilenumbruch mit '=' aus, zeitlich
            // verzögert um 250 ms
            rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetragMain;  //Methodenaufruf Ausgabe Rückgabebetrag
            // berechnet Rückgeld

            System.out.println(rueckgabeBetragAusgabe(rueckgabebetrag));

            System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                    "vor Fahrtantritt entwerten zu lassen!\n"+
                    "Wir wünschen Ihnen eine gute Fahrt.\n" +
                    "Möchten Sie noch mehr Fahrkarten kaufen? [ja/nein]");

            while(true) {
                Scanner eingabe = new Scanner(System.in);
                String mehrKaufen = eingabe.nextLine();
                if (mehrKaufen.equals("nein")) {
                    return;
                } else if (mehrKaufen.equals("ja")){
                    for (int i = 0; i < 8; i++)
                    {
                        System.out.print("=");
                        try {
                            Thread.sleep(250);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println("\n\n");
                    break;
                } else {
                    System.out.println("Bitte 'ja' oder 'nein' eingeben!");
                }
            }
        }
    }
}

